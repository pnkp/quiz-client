import { Routes } from '@angular/router';
import { JoinComponent } from '../components/start/join/join.component';
import { gameRoutes } from './gameRoutes';

export const mainRoutes: Routes = [
  {path: '', component: JoinComponent},
  ...gameRoutes
];
