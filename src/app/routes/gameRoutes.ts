import { Routes } from '@angular/router';
import { SelectCategoryComponent } from '../components/game/select-category/select-category.component';
import { WaitForPlayersComponent } from '../components/game/wait-for-players/wait-for-players.component';

export const gameRoutes: Routes = [
  {path: 'game/select-category', component: SelectCategoryComponent},
  {path: 'game/wait', component: WaitForPlayersComponent},
];
