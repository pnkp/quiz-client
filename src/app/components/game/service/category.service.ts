import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { SocketService } from '../../../services/socket.service';
import { JoinGameService } from '../../start/service/join-game.service';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(
    private readonly socketService: SocketService,
    private readonly http: HttpClient,
    private readonly joinGameService: JoinGameService) {
  }

  public getCategory() {
    this.http.get<any>(`${environment.apiUrl}`);
  }
}
