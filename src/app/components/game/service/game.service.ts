import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { PlayerService } from '../../../services/player.service';
import { SocketChanelEnum } from '../../../services/socket-chanel.enum';
import { SocketService } from '../../../services/socket.service';
import { Player } from '../../../types/Player';
import { ConfirmationCounterResponse } from '../types/ConfirmationCounterResponse';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  constructor(
    private readonly socketService: SocketService,
    private readonly http: HttpClient,
    private readonly playerService: PlayerService
  ) {
  }

  public getGameState(): Observable<any> {
    const { id } = this.playerService.getPlayer();

    const headers: HttpHeaders = new HttpHeaders({
      'Player-Id': id,
    });

    return this.http.get<any>(`${environment.apiUrl}/games`, {headers});
  }

  public confirmGame(): Observable<{ confirmed: true }> {
    return this.socketService.emit(SocketChanelEnum.ConfirmGame, {confirmed: true});
  }

  public confirmationFinished(): Observable<unknown> {
    return this.socketService.listen(SocketChanelEnum.WaitForPlayers)
      .pipe(map(({response}) => response));
  }

  public getPlayersInGame(): Observable<Player[]> {
    return this.socketService.emit(SocketChanelEnum.GetPlayersInGame).pipe(
      switchMap(() => this.socketService.listen<Player[]>(SocketChanelEnum.PlayersInGame)),
      map(({response}) => response),
    );
  }

  public runConfirmation(response: ConfirmationCounterResponse): Observable<any> {
    return this.socketService.emit(SocketChanelEnum.RunCounterSynchronisation, response);
  }

  public getConfirmationCounter(): Observable<ConfirmationCounterResponse> {
    return this.socketService.listen<ConfirmationCounterResponse>(SocketChanelEnum.GetConfirmationCounter)
      .pipe(map(({response}) => response));
  }
}
