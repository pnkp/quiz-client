export class ConfirmationCounterResponse {
  isRunning: boolean;
  counter: number;
}
