import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { GameService } from '../service/game.service';

@Component({
  selector: 'app-select-category',
  templateUrl: './select-category.component.html',
  styleUrls: ['./select-category.component.css']
})
export class SelectCategoryComponent implements OnInit {

  constructor(private readonly route: ActivatedRoute, private readonly gameService: GameService) {
  }

  ngOnInit() {
    this.route.paramMap.pipe(
      switchMap((params: ParamMap) => params.get('level'))
    ).subscribe((level) => console.log({level}));

    this.gameService.getGameState()
      .subscribe(state => console.log({state}));
  }

}
