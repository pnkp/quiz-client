import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { from, interval, race, Subject, Subscription, throwError } from 'rxjs';
import { map, mapTo, share, switchMap, takeUntil, takeWhile, toArray } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { Player } from '../../../types/Player';
import { GameService } from '../service/game.service';

@Component({
  selector: 'app-wait-for-players',
  templateUrl: './wait-for-players.component.html',
  styleUrls: ['./wait-for-players.component.css']
})
export class WaitForPlayersComponent implements OnInit, OnDestroy {

  public players: Player[];
  public isConfirmInDuring: boolean;
  public timerNumber: number;

  private isConfirmationCanceled: Subject<boolean> = new Subject<boolean>();
  private subs: Subscription = new Subscription();
  private timerSubscription: Subscription;
  private isConfirmInDuringByOtherPlayer: boolean;
  private confirmationCounterSubscription: Subscription;

  constructor(
    private readonly gameService: GameService,
    private readonly router: Router
  ) {
  }

  ngOnInit() {
    this.timerNumber = environment.timerCountToConfirmation;

    const playersSub = this.gameService.getPlayersInGame()
      .subscribe((players) => this.players = players);

    const somePlayerConfirmedGameSub = this.gameService.confirmationFinished()
      .subscribe(() => this.router.navigate(['']));

    const confirmationCounterListenerSub = this.gameService.getConfirmationCounter()
      .subscribe((confirmation) => {
        this.isConfirmInDuring = confirmation.isRunning;
        this.isConfirmInDuringByOtherPlayer = confirmation.isRunning;
        this.timerNumber = confirmation.counter;
      });

    this.subs
      .add(playersSub)
      .add(confirmationCounterListenerSub)
      .add(somePlayerConfirmedGameSub);
  }

  cancelConfirmation() {
    this.isConfirmInDuring = false;
    this.timerSubscription.unsubscribe();
    this.confirmationCounterSubscription.unsubscribe();
    this.isConfirmationCanceled.next(true);

    const runConfirmationSub = this.gameService.runConfirmation({isRunning: false, counter: undefined}).subscribe();
    this.subs.add(runConfirmationSub);
  }

  startConfirmation() {
    if (this.isConfirmInDuringByOtherPlayer) {
      return;
    }

    this.isConfirmInDuring = true;
    const confirmationTime = interval(1000).pipe(share());

    const timer$ = confirmationTime
      .pipe(takeWhile(time => time < environment.timerCountToConfirmation))
      .pipe(map(time => {
        const baseValueTimer = environment.timerCountToConfirmation;
        return baseValueTimer - (time + 1);
      }));

    this.confirmationCounterSubscription = timer$
      .pipe(switchMap(timer => this.gameService.runConfirmation({isRunning: true, counter: timer})))
      .subscribe(() => console.log('Timer synchronised'));
    this.subs.add(this.confirmationCounterSubscription);

    this.timerSubscription = timer$.subscribe(timer => this.timerNumber = timer);
    this.subs.add(this.timerSubscription);

    const canceled$ = confirmationTime.pipe(takeUntil(this.isConfirmationCanceled), toArray(), mapTo(false));
    const confirmed$ = confirmationTime.pipe(
      takeWhile(time => time < environment.timerCountToConfirmation),
      toArray(),
      mapTo(true)
    );

    const result$ = race(canceled$, confirmed$)
      .pipe(
        switchMap(confirmed => {
          if (!confirmed) {
            return throwError('Game confirmation was canceled');
          }

          return this.gameService.confirmGame();
        }))
      .subscribe(
        async () => {
          await this.router.navigate(['game/select-category', { level: 1 }]);
          console.log({confirm: 'Game confirmed'});
        },
        (err) => console.log({err})
      );
    this.subs.add(result$);
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
    this.isConfirmationCanceled.complete();
    this.isConfirmationCanceled.unsubscribe();
  }

}
