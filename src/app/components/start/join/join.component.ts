import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { from, Subscription } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { PlayerService } from '../../../services/player.service';
import { SocketService } from '../../../services/socket.service';
import { Player } from '../../../types/Player';
import { JoinGameService } from '../service/join-game.service';

@Component({
  selector: 'app-join',
  templateUrl: './join.component.html',
  styleUrls: ['./join.component.css']
})
export class JoinComponent implements OnInit, OnDestroy {
  public joinForm: FormGroup;
  public rooms: string[];
  public subs: Subscription = new Subscription();
  public isNewRoomCreating: boolean;

  constructor(
    private readonly socketService: SocketService,
    private readonly joinGameService: JoinGameService,
    private readonly router: Router,
    private readonly playerService: PlayerService
  ) {
  }

  ngOnInit() {
    this.isNewRoomCreating = false;

    const roomListSub = this.joinGameService.getRooms()
      .subscribe((rooms) => this.rooms = rooms);
    this.subs.add(roomListSub);

    this.joinForm = new FormGroup({
      playerName: new FormControl('', [
        Validators.required
      ]),
      room: new FormControl('', [
        Validators.required
      ])
    });
  }

  join() {
    if (!this.joinForm.valid) {
      console.warn('Form not valid');
      return;
    }

    const user = {
      playerName: this.joinForm.get('playerName').value,
      room: this.joinForm.get('room').value
    };

    const joinToGameSub = this.joinGameService.joinToGame<Player>(user)
      .pipe(map((player) => this.playerService.savePlayer(player)))
      .pipe(switchMap(() => from(this.router.navigate(['/game/wait']))))
      .subscribe(null, error => console.log({error}));

    this.subs.add(joinToGameSub);
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
