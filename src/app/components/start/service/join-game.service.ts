import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { SocketChanelEnum } from '../../../services/socket-chanel.enum';
import { SocketService } from '../../../services/socket.service';

@Injectable({
  providedIn: 'root'
})
export class JoinGameService {

  constructor(private readonly http: HttpClient, private readonly socketService: SocketService) {
  }

  public getRooms(): Observable<string[]> {
    return this.http.get<string[]>(`${environment.apiUrl}/rooms`);
  }

  public joinToGame<T>(user: { playerName: string; room: string }): Observable<T> {
    return new Observable((observer) => {
      this.socketService.getConnection().emit(SocketChanelEnum.Join, user, (callback) => {
        if (callback.error) {
          observer.error(callback.error);
        } else {
          observer.next(callback);
        }
      });
    });
  }

}
