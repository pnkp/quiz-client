import { TestBed } from '@angular/core/testing';

import { JoinGameService } from './join-game.service';

describe('JoinGameService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JoinGameService = TestBed.get(JoinGameService);
    expect(service).toBeTruthy();
  });
});
