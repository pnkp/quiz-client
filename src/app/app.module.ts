import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { JoinComponent } from './components/start/join/join.component';
import { mainRoutes } from './routes/mainRoutes';
import { SelectCategoryComponent } from './components/game/select-category/select-category.component';
import { WaitForPlayersComponent } from './components/game/wait-for-players/wait-for-players.component';

@NgModule({
  declarations: [
    AppComponent,
    JoinComponent,
    SelectCategoryComponent,
    WaitForPlayersComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    RouterModule.forRoot(mainRoutes, {enableTracing: false}),
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
