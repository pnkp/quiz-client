import { Injectable } from '@angular/core';
import { Player } from '../types/Player';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {
  private readonly playerStorageKey = 'player';

  constructor() { }

  public savePlayer(player: Player): void {
    localStorage.setItem(this.playerStorageKey, JSON.stringify(player));
  }

  public getPlayer(): Player {
    const {player} = JSON.parse(localStorage.getItem(this.playerStorageKey));
    if (!player) {
      throw new Error('Player not found');
    }

    return player;
  }
}
