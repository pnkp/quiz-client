export enum SocketChanelEnum {
  RoomList = 'roomList',
  CreateRoom = 'createRoom',
  Join = 'join',
  WaitForPlayers = 'waitForPlayers',
  PlayersInGame = 'playersInGame',
  GetPlayersInGame = 'getPlayersInGame',
  ConfirmGame = 'confirmGame',
  GetConfirmationCounter = 'getConfirmationCounter',
  RunCounterSynchronisation = 'runCounterSynchronisation',
}
