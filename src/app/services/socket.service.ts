import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { SocketChanelEnum } from './socket-chanel.enum';
import * as io from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  private readonly socketIO: SocketIOClient.Socket;

  constructor() {
    this.socketIO = io(environment.socketUrl);
  }

  public listen<Response>(chanel: SocketChanelEnum): Observable<{response: Response}> {
    return new Observable((observer) => {
      this.socketIO.on(chanel, (response, callback) => observer.next({response}));
    });
  }

  public emit<Data>(chanel: SocketChanelEnum, message?: Data): Observable<Data> {
    return new Observable<Data>((observer) => {
      this.socketIO.emit(chanel, message);
      observer.next(message);
      observer.complete();
    });
  }

  public getConnection(): SocketIOClient.Socket {
    return this.socketIO;
  }
}
